﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Player
    {
        private int posX;
        private int posY;
        private string looks;
        private int dir;
        
        public Player (int playerNum)
        {
            dir = 0;
            posX = 1;
            posY = 1;
            if (playerNum == 1)
                looks = "P1";
            else
                looks = "P2";
        }
        public void Draw()
        {
            Console.SetCursorPosition(posX, posY);
            Console.Write(looks);
        }
        public void MoveRight ()
        {
            dir = 0;
            if (posX < Console.WindowWidth)
                posX += 1;
        }
        public void MoveLeft()
        {
            dir = 2;
            if (posX > 0)
                posX -= 1;
        }
        public void MoveUp()
        {
            dir = 1;
            if (posY > 0)
                posY -= 1;
        }
        public void MoveDown()
        {
            dir = 3;
            if (posY < Console.WindowHeight)
                posY += 1;
        }
        public int GetPosX()
        {
            return posX;
        }
        public int GetPosY()
        {
            return posY;
        }
        public Bala shoot()
        {
            return new Bala(posX, posY, dir);
        }
    }
}
