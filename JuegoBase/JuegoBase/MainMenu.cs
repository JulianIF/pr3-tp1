﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class MainMenu
    {
        private ConsoleKeyInfo userKey;
        public void Run()
        {
            userKey = new ConsoleKeyInfo();
            Console.SetCursorPosition(50, 10);
            Console.WriteLine("Enter - Jugar");
            Console.SetCursorPosition(50, 12);
            Console.WriteLine("Esc - Salir");

            bool keyPressed = false;
            do
            {
                    userKey = Console.ReadKey();
                if (userKey.Key == ConsoleKey.Enter || userKey.Key == ConsoleKey.Escape)
                    keyPressed = true;

            } while (!keyPressed);


            Console.Clear();
            if (userKey.Key == ConsoleKey.Enter)
            {
                Console.SetCursorPosition(50, 10);
                Console.WriteLine("Enter - 1 Jugador");
                Console.SetCursorPosition(50, 12);
                Console.WriteLine("Spacebar - 2 Jugador");
                keyPressed = false;
                do
                {
                        userKey = Console.ReadKey();
                    if (userKey.Key == ConsoleKey.Enter || userKey.Key == ConsoleKey.Spacebar)
                        keyPressed = true;
                } while (!keyPressed);

                GameLoop game = new GameLoop();
                if (userKey.Key == ConsoleKey.Enter)
                {
                    game.Run(false);
                }
                else
                {
                    game.Run(true);
                }
            }
        }
    }
}
