﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Obstacle
    {
        private int posX;
        private int posY;
        private char looks;
        

        public void Draw()
        {
            Console.SetCursorPosition(posX, posY);
            Console.Write(looks);
        }

        public Obstacle()
        {
            posX = GameLoop.r.Next(0, Console.WindowWidth);
            posY = GameLoop.r.Next(0, Console.WindowHeight);
            looks = 'O';
        }
        public int GetPosX()
        {
            return posX;
        }
        public int GetPosY()
        {
            return posY;
        }
    }
}
