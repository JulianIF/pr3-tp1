﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Bala
    {
        private int posX;
        private int posY;
        private string looks;
        private int dir;
        private bool kill;

        public Bala(int x, int y, int _dir)
        {
            kill = false;
            posX = x;
            posY = y;
            dir = _dir;
            switch(dir)
            {
                case 0:
                    looks = "->";
                    break;
                case 1:
                    looks = @"/\";
                    break;
                case 2:
                    looks = "<-";
                    break;
                case 3:
                    looks = @"\/";
                    break;
            }
        }

        public Bala()
        {
            kill = true;
            posX = 0;
            posY = 0;
            dir = 0;
            looks = " ";
        }

        public int GetPosX()
        {
            return posX;
        }

        public int GetPosY()
        {
            return posY;
        }

        public bool isDead()
        {
            return kill;
        }

        public void killDaBala()
        {
            kill = true;
            posX = 0;
            posY = 0;
            dir = 0;
            looks = " ";
        }

        private void Draw()
        {
            if (!kill)
            {
                Console.SetCursorPosition(posX, posY);
                Console.Write(looks);
            }
        }

        public void update()
        {
            switch (dir)
            {
                case 0:
                    if (posX < Console.WindowWidth)
                        posX++;
                    else
                        kill = true;
                    break;
                case 1:
                    if (posY > 0)
                        posY--;
                    else
                        kill = true;
                    break;
                case 2:
                    if (posX > 0)
                        posX--;
                    else
                        kill = true;
                    break;
                case 3:
                    if (posY < Console.WindowHeight)
                        posY++;
                    else
                        kill = true;
                    break;
            }
            if(kill)
            {
                looks = " ";
            }
            Draw();
        }
    }
}
