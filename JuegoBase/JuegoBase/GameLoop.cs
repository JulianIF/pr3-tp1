﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class GameLoop
    {
        private bool coop;
        static public Random r = new Random();
        public void Run(bool coopEnabled)
        {
            coop = coopEnabled;
            int gameRunning = 3;
            Bala b = new Bala();
            Player p1 = new Player(1);
            Player p2 = new Player(2);
            Enemy e = new Enemy();
            Obstacle[] o = new Obstacle[20];
            for (int i = 0; i < o.Length; i++)
            {
                o[i] = new Obstacle();
            }
            ConsoleKeyInfo userKey;
            while (gameRunning != 0)
            {
                e.Move();
                if (Console.KeyAvailable && !coop)
                {
                    userKey = Console.ReadKey(true);

                    switch (userKey.Key)
                    {
                        case ConsoleKey.Escape:
                            gameRunning = 0;
                            break;
                        case ConsoleKey.D:
                            p1.MoveRight();
                            break;
                        case ConsoleKey.A:
                            p1.MoveLeft();
                            break;
                        case ConsoleKey.S:
                            p1.MoveDown();
                            break;
                        case ConsoleKey.W:
                            p1.MoveUp();
                            break;
                        case ConsoleKey.Spacebar:
                            if(b.isDead())
                            {
                                b = p1.shoot();
                            }
                            break;
                    }
                }
                else if (Console.KeyAvailable && coop)
                {
                    userKey = Console.ReadKey(true);

                    switch (userKey.Key)
                    {
                        case ConsoleKey.Escape:
                            gameRunning = 0;
                            break;
                        case ConsoleKey.D:
                            p1.MoveRight();
                            break;
                        case ConsoleKey.A:
                            p1.MoveLeft();
                            break;
                        case ConsoleKey.S:
                            p1.MoveDown();
                            break;
                        case ConsoleKey.W:
                            p1.MoveUp();
                            break;
                        case ConsoleKey.UpArrow:
                            p2.MoveUp();
                            break;
                        case ConsoleKey.DownArrow:
                            p2.MoveDown();
                            break;
                        case ConsoleKey.LeftArrow:
                            p2.MoveLeft();
                            break;
                        case ConsoleKey.RightArrow:
                            p2.MoveRight();
                            break;
                    }
                }
                Console.Clear();
                b.update();
                p1.Draw();
                if (coop)
                    p2.Draw();
                e.Draw();
                if (b.GetPosX() == e.GetPosX() && b.GetPosY() == e.GetPosY())
                {
                    e = new Enemy();
                    b.killDaBala();
                }
                    for (int i = 0; i < o.Length - 1; i++)
                {
                    o[i].Draw();
                }
                for (int i = 0; i < o.Length; i++)
                {
                    if (p1.GetPosX() == o[i].GetPosX() && p1.GetPosY() == o[i].GetPosY() || p1.GetPosX() == e.GetPosX() && p1.GetPosY() == e.GetPosY())
                    {
                        gameRunning -= 1;
                        if (gameRunning == 0)
                        {
                            Console.Clear();
                            Console.SetCursorPosition(50, 10);
                            if (coop)
                                Console.WriteLine("Gana el Segundo Jugador");
                            else
                                Console.WriteLine("Perdiste");
                            Console.ReadKey();
                        }
                        else
                        {
                            p1 = new Player(1);
                        }
                         
                    }
                    else if (coop && p2.GetPosX() == o[i].GetPosX() && p2.GetPosY() == o[i].GetPosY() || p2.GetPosX() == e.GetPosX() && p2.GetPosY() == e.GetPosY())
                    {
                        gameRunning -= 1;
                        if (gameRunning == 0)
                        {
                            Console.Clear();
                            Console.SetCursorPosition(50, 10);
                            Console.WriteLine("Gana el Primer Jugador");
                            Console.ReadKey();
                        }
                        else
                        {
                            p2 = new Player(2);
                        }
                    }
                }
                Console.SetCursorPosition(0, 0);
                if (gameRunning > 0)
                {
                    for (int i = 0; i < gameRunning; i++)
                    {
                        Console.Write("<3 ");
                    }
                }
                System.Threading.Thread.Sleep(200);
            }
        }
    }
}
